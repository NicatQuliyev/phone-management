package az.nicat.phonebase.repository;

import az.nicat.phonebase.entity.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactRepository extends JpaRepository<Contact, Long> {

    Contact findByName(String name);
    Contact findBySurname(String surname);
}
