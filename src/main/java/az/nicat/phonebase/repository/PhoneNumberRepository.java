package az.nicat.phonebase.repository;

import az.nicat.phonebase.entity.PhoneNumber;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, Long> {

    PhoneNumber findByNumber(String number);

    Boolean existsByNumber(String number);
}
