package az.nicat.phonebase.service;

import az.nicat.phonebase.dto.request.PhoneNumberRequest;
import az.nicat.phonebase.dto.response.PhoneNumberResponse;
import az.nicat.phonebase.entity.Contact;
import az.nicat.phonebase.entity.PhoneNumber;
import az.nicat.phonebase.exception.ContactNotFoundException;
import az.nicat.phonebase.exception.ErrorCodes;
import az.nicat.phonebase.exception.PhoneNumberExistException;
import az.nicat.phonebase.exception.PhoneNumberNotFoundException;
import az.nicat.phonebase.repository.ContactRepository;
import az.nicat.phonebase.repository.PhoneNumberRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class PhoneNumberService {

    private final PhoneNumberRepository numberRepository;

    private final ContactRepository contactRepository;

    private final ModelMapper modelMapper;

    public List<PhoneNumberResponse> findAll() {
        return numberRepository
                .findAll()
                .stream()
                .map(number -> modelMapper.map(number, PhoneNumberResponse.class))
                .collect(Collectors.toList());
    }

    public PhoneNumberResponse findById(Long numberId) {
        PhoneNumber number = numberRepository.findById(numberId).orElseThrow(() ->
                new PhoneNumberNotFoundException(ErrorCodes.NUMBER_NOT_FOUND));

        return modelMapper.map(number, PhoneNumberResponse.class);
    }

    public PhoneNumberResponse findByNumber(String number) {

        PhoneNumber phoneNumber = numberRepository.findByNumber(number);

        if (phoneNumber == null) {
            throw new PhoneNumberNotFoundException(ErrorCodes.NUMBER_NOT_FOUND);
        }

        return modelMapper.map(phoneNumber, PhoneNumberResponse.class);
    }

    public PhoneNumberResponse save(PhoneNumberRequest numberRequest, Long contactId) {

        if (numberRepository.existsByNumber(numberRequest.getNumber())) {
            throw new PhoneNumberExistException(ErrorCodes.NUMBER_ALREADY_EXIST);
        }

        Contact contact = contactRepository.findById(contactId).orElseThrow(() ->
                new ContactNotFoundException(ErrorCodes.CONTACT_NOT_FOUND));


        PhoneNumber number = modelMapper.map(numberRequest, PhoneNumber.class);
        number.setContact(contact);


        return modelMapper.map(numberRepository.save(number), PhoneNumberResponse.class);
    }

    public PhoneNumberResponse update(PhoneNumberRequest numberRequest, Long numberId) {
        numberRepository.findById(numberId).orElseThrow(() ->
                new PhoneNumberNotFoundException(ErrorCodes.NUMBER_NOT_FOUND));

        PhoneNumber number = modelMapper.map(numberRequest, PhoneNumber.class);
        number.setId(numberId);

        return modelMapper.map(numberRepository.save(number), PhoneNumberResponse.class);
    }

    public void delete(Long numberId) {
        PhoneNumber number = numberRepository.findById(numberId).orElseThrow(() ->
                new PhoneNumberNotFoundException(ErrorCodes.NUMBER_NOT_FOUND));

        numberRepository.delete(number);
    }
}
