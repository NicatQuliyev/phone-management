package az.nicat.phonebase.service;

import az.nicat.phonebase.dto.request.ContactRequest;
import az.nicat.phonebase.dto.response.ContactResponse;
import az.nicat.phonebase.entity.Contact;
import az.nicat.phonebase.exception.ContactNotFoundException;
import az.nicat.phonebase.exception.ErrorCodes;
import az.nicat.phonebase.repository.ContactRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ContactService {

    private final ContactRepository contactRepository;

    private final ModelMapper modelMapper;

    public List<ContactResponse> findAll() {
        return contactRepository
                .findAll()
                .stream()
                .map(contact -> modelMapper.map(contact, ContactResponse.class))
                .collect(Collectors.toList());
    }

    public ContactResponse findById(Long contactId) {
        Contact contact = contactRepository.findById(contactId).orElseThrow(() ->
                new ContactNotFoundException(ErrorCodes.CONTACT_NOT_FOUND));

        return modelMapper.map(contact, ContactResponse.class);
    }

    public ContactResponse findByName(String name) {
        Contact contact = contactRepository.findByName(name);

        if (contact == null) {
            throw new ContactNotFoundException(ErrorCodes.CONTACT_NOT_FOUND);
        }

        return modelMapper.map(contact, ContactResponse.class);
    }

    public ContactResponse findBySurname(String surname) {
        Contact contact = contactRepository.findBySurname(surname);

        if (contact == null) {
            throw new ContactNotFoundException(ErrorCodes.CONTACT_NOT_FOUND);
        }

        return modelMapper.map(contact, ContactResponse.class);
    }

    public ContactResponse save(ContactRequest request) {
        Contact contact = modelMapper.map(request, Contact.class);

        return modelMapper.map(contactRepository.save(contact), ContactResponse.class);
    }

    public ContactResponse update(Long contactId, ContactRequest request) {
        contactRepository.findById(contactId).orElseThrow(() ->
                new ContactNotFoundException(ErrorCodes.CONTACT_NOT_FOUND));

        Contact contact = modelMapper.map(request, Contact.class);
        contact.setId(contactId);

        return modelMapper.map(contactRepository.save(contact), ContactResponse.class);
    }

    public void delete(Long contactId) {
        Contact contact = contactRepository.findById(contactId).orElseThrow(() ->
                new ContactNotFoundException(ErrorCodes.CONTACT_NOT_FOUND));

        contactRepository.delete(contact);
    }
}
