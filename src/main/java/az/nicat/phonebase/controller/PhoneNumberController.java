package az.nicat.phonebase.controller;

import az.nicat.phonebase.dto.request.PhoneNumberRequest;
import az.nicat.phonebase.dto.response.PhoneNumberResponse;
import az.nicat.phonebase.service.PhoneNumberService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/numbers")
@RequiredArgsConstructor
@Slf4j
public class PhoneNumberController {

    private final PhoneNumberService numberService;

    @GetMapping
    public ResponseEntity<List<PhoneNumberResponse>> findAll() {
        return new ResponseEntity<>(numberService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{numberId}")
    public ResponseEntity<PhoneNumberResponse> findById(@PathVariable Long numberId) {
        return new ResponseEntity<>(numberService.findById(numberId), HttpStatus.OK);
    }

    @GetMapping("/number")
    public ResponseEntity<PhoneNumberResponse> findByName(@RequestParam String number) {
        return new ResponseEntity<>(numberService.findByNumber(number), HttpStatus.OK);
    }

    @PostMapping("/{contactId}")
    public ResponseEntity<PhoneNumberResponse> save(@RequestBody PhoneNumberRequest request,
                                                    @PathVariable Long contactId) {
        return new ResponseEntity<>(numberService.save(request, contactId), HttpStatus.CREATED);
    }

    @PutMapping("/{numberId}")
    public ResponseEntity<PhoneNumberResponse> update(@RequestBody PhoneNumberRequest request,
                                                      @PathVariable Long numberId) {
        return new ResponseEntity<>(numberService.update(request, numberId), HttpStatus.OK);
    }

    @DeleteMapping("/{numberId}")
    public void delete(@PathVariable Long numberId) {
        numberService.delete(numberId);
    }
}
