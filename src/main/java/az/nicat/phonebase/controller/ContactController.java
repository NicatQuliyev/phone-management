package az.nicat.phonebase.controller;

import az.nicat.phonebase.dto.request.ContactRequest;
import az.nicat.phonebase.dto.response.ContactResponse;
import az.nicat.phonebase.service.ContactService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/contacts")
@RequiredArgsConstructor
@Slf4j
public class ContactController {


    private final ContactService contactService;

    @GetMapping
    public ResponseEntity<List<ContactResponse>> findAll() {
        return new ResponseEntity<>(contactService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{contactId}")
    public ResponseEntity<ContactResponse> findById(@PathVariable Long contactId) {
        return new ResponseEntity<>(contactService.findById(contactId), HttpStatus.OK);
    }

    @GetMapping("/name")
    public ResponseEntity<ContactResponse> findByName(@RequestParam String name) {
        return new ResponseEntity<>(contactService.findByName(name), HttpStatus.OK);
    }

    @GetMapping("/surname")
    public ResponseEntity<ContactResponse> findBySurname(@RequestParam String surname) {
        return new ResponseEntity<>(contactService.findBySurname(surname), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ContactResponse> save(@RequestBody ContactRequest request) {
        return new ResponseEntity<>(contactService.save(request), HttpStatus.CREATED);
    }

    @PutMapping("/{contactId}")
    public ResponseEntity<ContactResponse> update(@RequestBody ContactRequest request,
                                                  @PathVariable Long contactId) {
        return new ResponseEntity<>(contactService.update(contactId, request), HttpStatus.OK);
    }

    @DeleteMapping("/{contactId}")
    public void delete(@PathVariable Long contactId) {
        contactService.delete(contactId);
    }
}
