package az.nicat.phonebase.dto.request;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ContactRequest {

    String name;

    String surname;

    Integer age;

    String email;

    String company;
}
