package az.nicat.phonebase.dto.response;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class ContactResponse {

    Long id;

    String name;

    String surname;

    Integer age;

    String email;

    String company;

//    List<PhoneNumberResponse> phoneNumbers = new ArrayList<>();
}
