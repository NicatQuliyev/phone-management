package az.nicat.phonebase.dto.response;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class PhoneNumberResponse {

    Long id;

    String number;

    ContactResponse contact;
}
