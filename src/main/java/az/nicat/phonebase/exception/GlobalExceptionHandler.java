package az.nicat.phonebase.exception;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {

    @ExceptionHandler(ContactExistException.class)
    public ResponseEntity<ErrorResponseDto> handleUserNameExistException(ContactExistException ex,
                                                                         WebRequest req) {

        ex.printStackTrace();

        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .details("Contact already exist")
                .build());
    }

    @ExceptionHandler(PhoneNumberExistException.class)
    public ResponseEntity<ErrorResponseDto> handlePhoneNumberExistException(PhoneNumberExistException ex,
                                                                            WebRequest req) {

        ex.printStackTrace();

        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .details("Phone number already exist")
                .build());
    }


    @ExceptionHandler(ContactNotFoundException.class)
    public ResponseEntity<ErrorResponseDto> handleUserNotFoundException(ContactNotFoundException ex,
                                                                        WebRequest req) {
        ex.printStackTrace();

        return ResponseEntity.status(404).body(ErrorResponseDto.builder()
                .status(404)
                .title("Exception")
                .details("Contact not found")
                .build());
    }

    @ExceptionHandler(PhoneNumberNotFoundException.class)
    public ResponseEntity<ErrorResponseDto> handlePhoneNumberNotFoundException(PhoneNumberNotFoundException ex,
                                                                               WebRequest req) {
        ex.printStackTrace();

        return ResponseEntity.status(404).body(ErrorResponseDto.builder()
                .status(404)
                .title("Exception")
                .details("Phone number not found")
                .build());
    }


//    @ExceptionHandler(MethodArgumentNotValidException.class)
//    public ResponseEntity<ErrorResponseDto> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex,
//                                                                                  WebRequest req) {
//        ex.printStackTrace();
//        ErrorResponseDto response = ErrorResponseDto.builder()
//                .status(400)
//                .title("Exception")
//                .details("Validation Error")
//                .build();
//
//        ex.getBindingResult()
//                .getFieldErrors()
//                .stream()
//                .forEach(error -> {
//                    Map<String, String> data = response.getData();
//                    data.put(error.getField(), "password must be 6 or more alphanumeric characters.");
//                });
//        return ResponseEntity.status(400).body(response);
//    }
}
