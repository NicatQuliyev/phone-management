package az.nicat.phonebase.exception;

import lombok.Getter;

@Getter
public class PhoneNumberExistException extends RuntimeException {

    public final ErrorCodes errorCode;
    public final transient Object[] arguments;

    public PhoneNumberExistException(ErrorCodes errorCode, Object... args) {
        this.errorCode = errorCode;
        this.arguments = args == null ? new Object[0] : args;
    }
}
