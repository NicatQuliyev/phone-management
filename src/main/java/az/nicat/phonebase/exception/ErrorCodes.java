package az.nicat.phonebase.exception;

public enum ErrorCodes {
    CONTACT_ALREADY_EXIST,
    CONTACT_NOT_FOUND,
    NUMBER_ALREADY_EXIST,
    NUMBER_NOT_FOUND
}